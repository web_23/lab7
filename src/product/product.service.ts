import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Products } from './entities/product.entity';

let products: Products[] = [
  { id: 1, name: 'computer', price: 2500 },
  { id: 2, name: 'vegetable', price: 60 },
  { id: 3, name: 'washing machine', price: 1000 },
];

let lastProductId = 4;
@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    const newUser: Products = {
      id: lastProductId++,
      ...createProductDto, //name, price
    };
    products.push(newUser);
    return newUser;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Products = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }
  reset() {
    products = [
      { id: 1, name: 'cake', price: 30 },
      { id: 2, name: 'apple', price: 50 },
      { id: 3, name: 'banana', price: 20 },
    ];
    const lastProductId = 4;
    return 'RESET IS SUCCEED';
  }
}
